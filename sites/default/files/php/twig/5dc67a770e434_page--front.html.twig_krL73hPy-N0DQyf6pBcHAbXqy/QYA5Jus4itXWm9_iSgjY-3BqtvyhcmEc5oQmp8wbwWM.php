<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/KCORP2/page--front.html.twig */
class __TwigTemplate_961021b226c31f5411c976a86ce18b6ed4b31322780dd36abe9ea8e3b4c5ea89 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["include" => 2];
        $filters = ["escape" => 7];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['include'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo " 
";
        // line 2
        $this->loadTemplate((($context["directory"] ?? null) . "/header.html.twig"), "themes/KCORP2/page--front.html.twig", 2)->display($context);
        // line 3
        echo "             

 <section>
     <div class=\"slider\">
         ";
        // line 7
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "slider", [])), "html", null, true);
        echo "
     </div>
 </section>
 <section>
     <div class=\"KCORP-INTRODUCTION\">
         <div class=\"row\">
             <div class=\"col-md-6 col-sm-4 col-xs-12\">
                 ";
        // line 14
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "kcorp_introduction", [])), "html", null, true);
        echo "
                 <center>
                     <button class=\"blue-btn\">READ MORE</button>
                 </center>
             </div>
             <div class=\"col-md-6 col-sm-4 col-xs-12\">
                 <div class=\"kcorp-introduction-image\">
                     <img src=\"themes/KCORP2/assets/images/khusheim%20Web%20Design%20NEW.jpg\" class=\"img-responsive\">
                 </div>
             </div>
         </div>

     </div>
 </section>
 <section>
     <div class=\"offers\">
         <div class=\"container\">
             ";
        // line 31
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "offers", [])), "html", null, true);
        echo "
             ";
        // line 32
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "offers_item", [])), "html", null, true);
        echo "
         </div>

          </div>
 </section>
 <section>
     <div class=\"products\">
         <h2>PRODUCTS</h2>
         ";
        // line 40
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "products", [])), "html", null, true);
        echo "




     </div>

 </section>
 <section>
     ";
        // line 49
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "after_service", [])), "html", null, true);
        echo "
 </section>
 <section>
     <div class=\"qualfication\">
         <div class=\"container\">
                 ";
        // line 54
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "qualification", [])), "html", null, true);
        echo "
         </div>


     </div>
 </section>
 <section>
     <div class=\"help\">
         <div class=\"container\">
         ";
        // line 63
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "help", [])), "html", null, true);
        echo "
             <div class=\"help-form\">
                 <form action=\"\" class=\"form-inline\">

                     <div class=\"form-group\">
                         <input type=\"text\" style=\"width: 285px;\" class=\"form-control form-input\" placeholder=\"Name\">
                     </div>
                     <div class=\"form-group\">
                         <input style=\"width: 285px;\" stype=\"email\" class=\"form-control form-input\" placeholder=\"Email\">
                     </div>
                     <div class=\"form-group\">
                         <div class=\"help-button\">
                             <center>
                                 <button class=\"help-btn\">SUBMIT</button>
                             </center>
                         </div>
                     </div>
                 </form>
             </div>
         </div>
     </div>
 </section>
 <section>
     <div class=\"map-section\">
         <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3383.8151125847858!2d35.870220814599!3d31.993027130852173!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x151ca102cbe2787f%3A0xbdd6f87e40fa5da2!2sFikra%20for%20business%20development!5e0!3m2!1sen!2sjo!4v1573103897430!5m2!1sen!2sjo\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>
     </div>
 </section>
";
        // line 90
        $this->loadTemplate((($context["directory"] ?? null) . "/footer.html.twig"), "themes/KCORP2/page--front.html.twig", 90)->display($context);
    }

    public function getTemplateName()
    {
        return "themes/KCORP2/page--front.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  173 => 90,  143 => 63,  131 => 54,  123 => 49,  111 => 40,  100 => 32,  96 => 31,  76 => 14,  66 => 7,  60 => 3,  58 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/KCORP2/page--front.html.twig", "C:\\wamp64\\www\\kcorp2\\themes\\KCORP2\\page--front.html.twig");
    }
}
