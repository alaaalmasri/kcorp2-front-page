<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/KCORP2/footer.html.twig */
class __TwigTemplate_170504ebd1cbe47f72acaaad70bf139413624caf7dc020b2612c4a573856bc5b extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["escape" => 12];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "

 <footer>

     <div class=\"footer\">
         <div class=\"blue-background\">
             <div class=\"container\">
                 <div class=\"row\">

                     <div class=\"col-md-4 col-sm-3 col-xs-12\">
                         <div class=\"footer-contact\">
                           ";
        // line 12
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_desc", [])), "html", null, true);
        echo "
                         </div>
                         <div class=\"social-media\">
                           ";
        // line 15
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "social_media", [])), "html", null, true);
        echo "
                         </div>
                     </div>

                     <div class=\"col-md-4 col-sm-12 col-xs-12\">
                         <div class=\"footer-links\">
                         ";
        // line 21
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "explore_links", [])), "html", null, true);
        echo "
                         </div>
                     </div>

                     <div class=\"col-md-4 col-sm-12 col-xs-12\">

                         <div class=\"contact\">                         
                           ";
        // line 28
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_contact", [])), "html", null, true);
        echo "
                         </div>
                         <div class=\"footer-search\">
                             <center>
                                 <input type=\"text\" class=\"form-control\" placeholder=\"Search entier store here\">

                                 <button class=\"footer-search-button\"><i class=\"fa fa-search\"></i></button>
                             </center>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
     <div class=\"copyrght\">
         <p>Copright@Fikra All rights reserved</p>
     </div>
 </footer>
";
    }

    public function getTemplateName()
    {
        return "themes/KCORP2/footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 28,  83 => 21,  74 => 15,  68 => 12,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/KCORP2/footer.html.twig", "C:\\wamp64\\www\\kcorp2\\themes\\KCORP2\\footer.html.twig");
    }
}
