<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/KCORP2/header.html.twig */
class __TwigTemplate_9a18e5797b5dbbd01fe0b9dd2b20b7e11b64ca367ab60e26c96096e7409bd38f extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["escape" => 17];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "                   <div class=\"header\">

                            <div class=\"row\">
         <div class=\"col-md-3 col-sm-12 col-xs-12\">
             <div class=\"logo\">
                 <img src=\"themes/KCORP2/assets/images/Untitled-2.png\" class=\"img-responsive\">
             </div>
             <!--end of logo-->

         </div>
         <!--end of col-->


         <div class=\"col-md-9 col-sm-12 col-xs-12\">
             <div class=\"contact-information\">
                 <div class=\"container\">
                     ";
        // line 17
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "contact", [])), "html", null, true);
        echo "
                 </div>
                 <div class=\"search\">
                     <center>
                         <input type=\"text\" class=\"form-control\" placeholder=\"Search entier store here\">

                         <button class=\"search-button\"><i class=\"fa fa-search\"></i></button>
                     </center>
                 </div>
             </div>
             <!--end of contact-->
             <div class=\"main-nav\">
                 <nav class=\"navbar navbar-expand-lg navbar-light\">
                     <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                         <span class=\"navbar-toggler-icon\"></span>
                     </button>

                     <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
                    
                         ";
        // line 36
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "main_navbar", [])), "html", null, true);
        echo "
                        
                     </div>
                 </nav>
             </div>
         </div>
     </div>
     <!--end of row-->
 </div>
 <!--end of header-->";
    }

    public function getTemplateName()
    {
        return "themes/KCORP2/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 36,  73 => 17,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/KCORP2/header.html.twig", "C:\\wamp64\\www\\kcorp2\\themes\\KCORP2\\header.html.twig");
    }
}
