<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/KCORP2/page.html.twig */
class __TwigTemplate_d861bb7d398a0958424a8577d23ad046d8dff2786a1f8828df600c6b8fd88f82 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["include" => 1, "if" => 5];
        $filters = ["escape" => 9];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['include', 'if'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $this->loadTemplate((($context["directory"] ?? null) . "/header.html.twig"), "themes/KCORP2/page.html.twig", 1)->display($context);
        // line 2
        echo "<div id=\"main-content\">
    <div class=\"container\">
        <div class=\"row\">
         ";
        // line 5
        if ($this->getAttribute(($context["page"] ?? null), "side_bar", [])) {
            // line 6
            echo "            <div class=\"content-area col-md-9 col-sm-12 col-xs-12\">
                <section id=\"content\">
                    <div id=\"content-wrap\">
                        ";
            // line 9
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
            echo "
                    </div>
                </section>
            </div>
            <aside id=\"sidebar\" class=\"col-md-3 col-sm-12 col-xs-12\">
                ";
            // line 14
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "side_bar", [])), "html", null, true);
            echo "
            </aside>
             ";
        } else {
            // line 17
            echo "            <div class=\"content-area col-md-12 col-sm-12 col-xs-12\">
                <section id=\"content\">
                    <div id=\"content-wrap\">
                      ";
            // line 20
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
            echo "
                    </div>
                </section>
            </div>
           ";
        }
        // line 25
        echo "        </div>
    </div>
</div>
";
        // line 28
        $this->loadTemplate((($context["directory"] ?? null) . "/footer.html.twig"), "themes/KCORP2/page.html.twig", 28)->display($context);
    }

    public function getTemplateName()
    {
        return "themes/KCORP2/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 28,  96 => 25,  88 => 20,  83 => 17,  77 => 14,  69 => 9,  64 => 6,  62 => 5,  57 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/KCORP2/page.html.twig", "C:\\wamp64\\www\\kcorp2\\themes\\KCORP2\\page.html.twig");
    }
}
